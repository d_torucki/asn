﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ASN.Controllers;



namespace ASN.Models
{
    public class Package
    {
        #region Variables

        
        public int Weight { get; set; }
        public int Price { get; set; }
        public int MP1 { get; set; }
        public int MP2 { get; set; }

        [Required(ErrorMessage = "Proszę wybrać lokalizację")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Prosze podać długość")]
        [Range(0, 120, ErrorMessage = "Maksymalna długość to 120 cm")]
        public int Length { get; set; }

        [Required(ErrorMessage = "Proszę podać wysokość")]
        [Range(0, 120, ErrorMessage = "Maksymalna wysokość to 120 cm")]
        public int Height { get; set; }

        [Required(ErrorMessage = "Proszę podać szerokość")]
        [Range(0, 120, ErrorMessage = "Maksymalna szerokość to 120 cm")]
        public int Width { get; set; }



        #endregion

        public bool IsSizeInvalid()
        {
            if (((Length > 120 || (Width > 60 && Height > 60))
              || (Width > 120 || (Height > 60 && Length > 60))
              || (Height > 120 || (Length > 60 && Width > 60))))
            {
                return true;
            }
            else return false;
           
       }
        
        public void PackagePrice()
        {
            if (Country == "Polska")
                MP1 = 1;
            if (Country == "Unia Europejska")
                MP1 = 2;
            if (Country == "Pozostałe Kraje")
                MP1 = 3;

            if (Length <= 60)
                MP2 = 2;
            if (Length > 60)
                MP2 = 4;

            Price = MP1 * MP2 * Weight ;
        }

        public bool IsTooHeavy()
        {
            Random NewWeight = new Random();
            Weight = NewWeight.Next(1, 30);
            if (Weight >= 20)
            {
                return true;
            }
            else return false;
        }


    }
   
}