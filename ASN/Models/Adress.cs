﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ASN.Models
{
    public class Adress

        
    {

       

        [Required(ErrorMessage = "Proszę podać nazwę ulicy")]
        public string Street { get; set; }
        
        [Required(ErrorMessage = "Proszę podać miejscowość")]
        public string City { get; set; }

        [Required(ErrorMessage = "Proszę podać kod pocztowy")]
        //[RegularExpression("0-9+\\-.+\\..", ErrorMessage = "Podaj kod w formacie 00-000")]
        public string PostCode { get; set; }

        


    }
     
    
}