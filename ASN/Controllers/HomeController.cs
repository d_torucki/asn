﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASN.Models;


namespace ASN.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

    
        [HttpPost]
        public ActionResult Index(MainClass package)
            
        {

            
            Session["Street"] = package.Ad1.Street;
            Session["City"] = package.Ad1.City;
            Session["PostCode"] = package.Ad1.PostCode;
            Session["Country"] = package.Pg1.Country;
            
            

            if (package.Pg1.IsSizeInvalid())
            {
                ModelState.AddModelError(string.Empty, "Paczka jest za duża. Maksymalne wymiary to 120x60x60 cm");
                return View("Index");
            }

            if (package.Pg1.IsTooHeavy())
             return View("Repeat");
            

            if (ModelState.IsValid)
            {
                Session["Weight"] = package.Pg1.Weight;
                package.Pg1.PackagePrice();
                Session["Price"] = package.Pg1.Price;
                return View("Summary");
            }

            else return View();
            
        }

        //[HttpPost]
        //public ViewResult Adress(MainClass adress)

        //{

        //    if (ModelState.IsValid)
        //    {
        //        return View("Summary", adress);
        //    }

        //    else return View();

        //}

        //[HttpPost]
        //public ViewResult Repeat(MainClass adress)

        //{

        //    if (ModelState.IsValid)
        //    {
        //        return View("Summary", adress);
        //    }

        //    else return View();

        }
    }
}